﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
namespace Game1
{
    class Score
    {
        //Data

        int value = 0;
        Vector2 positon = new Vector2(10, 10);
        SpriteFont font = null;

        //----------------------------------------------------
        //behaviour
        public void LoadContenet(ContentManager content)
        {
            font = content.Load<SpriteFont>("mainfont");
        }

        public void AddScore(int toAdd)
        {
            //this adds the provided number to the current score
            value += toAdd;
        }
        //----------------------------------------------------

        //setting the name to spriteBatch
        public void Draw(SpriteBatch spriteBatch)
        {
            //draw the score to the screen using the font variable
            spriteBatch.DrawString(font, "score  " + value.ToString(), positon, Color.White);
        }
        public void resetscore()
        {
            value = 0;
        }
    }
}
