﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Game1
{
    class Timer
    {
        float timeRemaining = 0;
        Vector2 positon = new Vector2(25, 25);
        SpriteFont font = null;
        bool running = false;

        //delegates
        //the delegates definition states what type of functions are allowed for this delegate
        public delegate void TimeUp();
        public TimeUp ourTimerCallback;

        public void LoadContenet(ContentManager content)
        {
            font = content.Load<SpriteFont>("mainfont");
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            //draw the Timer to the screen using the font variable
            int timeInt = (int)timeRemaining;
            spriteBatch.DrawString(font, "Time Left:  " + timeInt.ToString(), positon, Color.White);
        }
        public void Update (GameTime gameTime)
        {
            if (running == true)
            {
                timeRemaining -= (float)gameTime.ElapsedGameTime.TotalSeconds;

                //if our time has run out 
                if (timeRemaining <= 0)
                {
                    //stop and do something
                    running = false;
                    timeRemaining = 0;

                    //the something to do
                    if (ourTimerCallback != null)
                        ourTimerCallback();
                }
            }
        }
        public void StartTimer()
        {
            running = true;
        }
        public void SetTimer(float newTime)
        {
            timeRemaining = newTime;
        }
        
    }
}
