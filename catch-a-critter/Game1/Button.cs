﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio; // this is needed for sound effects


namespace Game1
{
    class Button
    {
        Texture2D image = null;
        Vector2 postion = new Vector2(375,225);
        SoundEffect click;
        bool visable = true;

        //delegates
        //the delegates definition states what type of functions are allowed for this delegate
        public delegate void OnClick();
        public OnClick ourButtonCallback;

        public void LoadContent(ContentManager content)
        {
            image = content.Load<Texture2D>("button");
            click = content.Load<SoundEffect>("buttonClick");
        }

        public void Input()
        {
            //this checks if the mouse button is down
            MouseState currentState = Mouse.GetState();
            //get the bounding box for the critter
            Rectangle Bounds = new Rectangle((int)postion.X, (int)postion.Y, image.Width, image.Height);

            if (currentState.LeftButton == ButtonState.Pressed
                && Bounds.Contains(currentState.X, currentState.Y)
                && visable == true)
            {
                //we clicked the critter
                //play sfx
                click.Play();

                visable = false;
                
                //start the game
                if (ourButtonCallback != null)
                    ourButtonCallback();
            }

        }
        public void Draw(SpriteBatch spriteBatch)
        {
            if (visable == true)
            {
                spriteBatch.Draw(image, postion, Color.White);
            }
        }
        public void Show()
        {
            visable = true;
        }
    }
}
