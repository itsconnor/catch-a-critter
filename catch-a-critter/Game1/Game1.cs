﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Game1
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        const int MAX_CRITTERS = 20;
        Critter [] critterPool = new Critter [MAX_CRITTERS]; //this makes and array of crttier refrences 
        Score ourScore; // a blank address for the players score
        const float SPAWN_DELAY = 3f; //this is the time delay between critter spwans
        float timeUntilNextSpawn = SPAWN_DELAY;
        int currentCritterIndex = 0;
        Button startButton = null;
        bool playing = false;
        Timer gameTimer = null;
        const float GAME_LENGTH = 5f;
        
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //create and load out button
            startButton = new Button();
            startButton.LoadContent(Content);
            //set the function that will be called when the button is clicked
            //( += means it adds to any existing functions set there)
            startButton.ourButtonCallback += StartGame;

            // TODO: use this.Content to load your game content here
            ourScore = new Score();
            ourScore.LoadContenet(Content);

            //create timer object and load its content
            gameTimer = new Timer();
            gameTimer.LoadContenet(Content);
            gameTimer.SetTimer(GAME_LENGTH);
            gameTimer.ourTimerCallback += endgame;

            //initialise random number generator for our critter species
            Random rnd = new Random();
     
            //create crittter objects and load the content
            for (int i = 0; i < MAX_CRITTERS; ++i)
            {
                Critter.Species newSpecies = (Critter.Species)rnd.Next(0, (int)Critter.Species.NUM);
                //here we are creating a new critter and giving it a score
                Critter newCritter = new Critter(ourScore, newSpecies);
                // this is us adding content for the new critter (image/sfx)
                newCritter.LoadContent(Content);

                //add the new critters to the pool
                critterPool[i] = newCritter;
            }
            

            //ourCritter.image = Content.Load<Texture2D>("panda");
            IsMouseVisible = true;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            //ourCritter.Input();
            startButton.Input();

            if (playing == true)
            {
                gameTimer.Update(gameTime);
                //should a new critter spawn?
                timeUntilNextSpawn -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (timeUntilNextSpawn <= 0f)
                {
                    //reset the spawn timer
                    timeUntilNextSpawn = SPAWN_DELAY;

                    //spawn a critter

                    //spawn the next critter in the lsit
                    critterPool[currentCritterIndex].Spawn(Window);
                    ++currentCritterIndex; //handle the wrap around
                                           //if we went past the end of the array, wrap back around to 0
                    if (currentCritterIndex >= critterPool.Length)
                    {
                        currentCritterIndex = 0;
                    }

                }
                foreach (Critter eachcritter in critterPool)
                {
                    //loop content
                    eachcritter.Input();
                }
            }
           
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            foreach (Critter eachcritter in critterPool)
            {
                //loop content
                eachcritter.Draw(spriteBatch);
            }
            startButton.Draw(spriteBatch);

            ourScore.Draw(spriteBatch);

            gameTimer.Draw(spriteBatch);

            spriteBatch.End();

            base.Draw(gameTime);
        }
        void StartGame()
        {
            playing = true;
            gameTimer.StartTimer();
            ourScore.resetscore();
        }
        void endgame()
        {
            playing = false;

            startButton.Show();

            foreach (Critter eachCritter in critterPool)
            {
                eachCritter.Despawn();
            }
            // reset timer
            gameTimer.SetTimer(GAME_LENGTH);
           
        }
        
    }
}
