﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio; // this is needed for sound effects


namespace Game1
{
    class Critter
    {
        public enum Species
            //this is a special type of integer that only holds spesific values allowed that have names
        {
            PANDA,  // =0
            SLOTH,      // =1
            BUFFALO,    // =2



            NUM        // =3
        }

        //----------------------------------------------------
        //this is our data
        Texture2D image = null;
        Vector2 postion = Vector2.Zero;
        SoundEffect clickSFX;
        bool Visable = false; //alive
        Score scoreObject = null;
        int critterValue = 10;
        Species ourSpecies = Species.PANDA;
        //----------------------------------------------------

        //Behaviour
        public Critter(Score newScore, Species newSpecies)
        {
            //this is a constructor this is called when the object is created
            //no return type (special)
            //helps us decide how the object will be set up
            //can have arguments (such as newScore)
            //this one lets us have access to the game's score

            scoreObject = newScore;
            ourSpecies = newSpecies;
        }
        public void LoadContent(ContentManager content)
        {
            clickSFX = content.Load<SoundEffect>("buttonClick");
            

            switch(ourSpecies)
            {
                case Species.PANDA:
                    image = content.Load<Texture2D>("panda");
                    critterValue = 10;
                    break;
                case Species.BUFFALO:
                    image = content.Load<Texture2D>("Buffalo");
                    critterValue = 15;
                    break;
                case Species.SLOTH:
                    image = content.Load<Texture2D>("Sloth");
                    critterValue = 20;
                    break;
                default:
                    //this should never happen 
                    break;
            }



        }
        //----------------------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            if (Visable == true)
            {
                spriteBatch.Draw(image, postion, Color.White);
            }
        }
        //----------------------------------------------------
        public void Spawn(GameWindow window)
        {
            //set the critter to visable
            Visable = true;
            //determining the bounds for the random location of the critter
            int postionYMin = 0;
            int postionXMin = 0;
            int postionYMax = window.ClientBounds.Height - image.Height;
            int postionXMax = window.ClientBounds.Width - image.Width;

            //generate random location of the critter withing the bounds
            Random rand = new Random();
            postion.X = rand.Next(postionXMin, postionXMax);
            postion.Y = rand.Next(postionYMin, postionYMax);
        }
        //----------------------------------------------------
        public void Despawn()
        {
            //set the critter to not alive in order to make it not draw and not clickable
            Visable = false;
        }

        public void Input()
        {
            //this checks if the mouse button is down
            MouseState currentState = Mouse.GetState();
            //get the bounding box for the critter
            Rectangle critterBounds = new Rectangle((int)postion.X,  (int)postion.Y,   image.Width,   image.Height);
            //check to see if the left button is held down over the critter
            if (currentState.LeftButton == ButtonState.Pressed 
                && critterBounds.Contains(currentState.X, currentState.Y) 
                && Visable == true)
            {
                //we clicked the critter
                //play sfx
                clickSFX.Play();
                //despawn the critter
                Despawn();

                //add to score (TODO)
                scoreObject.AddScore(critterValue);
            }
        }
    }
}
